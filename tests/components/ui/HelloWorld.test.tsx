import { describe, test, expect, vi } from 'vitest';
import { render } from '@testing-library/react';
import { HelloWorld } from '../../../src/components/ui/HelloWorld';
import React from "react";

describe('HelloWorld', () => {

    test('Should render', async () => {
        const { getByText, unmount } = render(<HelloWorld />);
        expect(getByText(/Hello/).textContent).toBe('This is your Hello World')
        unmount();
    });

    test('Should display hello with text', () => {
        const { getByText, unmount } = render(<HelloWorld text="Me" />);
        expect(getByText(/Me/).textContent).toBe('This is your Hello World for Me')
        unmount();
    });

});

