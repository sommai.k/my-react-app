import { FC } from 'react';
import { Landing } from './components/pages/Landing';

const App: FC = () => {
  return <>
    <Landing />
  </>
}

export default App;
