import { FC } from 'react';

interface Props {
    text: string;
}

export const BigText: FC<Props> = (props) => {
    return <div
        className='text-3xl text-blue-800 font-semibold tracking-widest uppercase border-4 px-2 rounded-lg hover:border-blue-600 shadow-sm'>{props.text}</div>;
}