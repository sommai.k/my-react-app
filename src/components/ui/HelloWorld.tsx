import { FC } from 'react';

interface Props {
    text?: string;
}

export const HelloWorld: FC<Props> = (props) => {
    const name = "Hello World";

    const sayName = (n: string) => {
        if (props.text) {
            return `This is your ${n} for ${props.text}`;
        }
        return `This is your ${n}`;
    }

    const element = <div>{sayName(name)}</div>;
    return element;
}