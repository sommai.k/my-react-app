import { FC, ReactNode } from 'react';

interface Props {
    children: ReactNode
}

export const Parent: FC<Props> = (props) => {
    return <div className='bg-gray-50 h-screen px-8 py-14'>
        {props.children}
    </div>
}