import { FC } from 'react';
import { Controller, Control } from 'react-hook-form';

interface Props {
    label: string;
    control: Control<any>;
    name: string;
    inputType?: string;
}

const inputElement = (field: any, name: string, inputType?: string) => {
    return <input
        id={name}
        className='border-blue-500 rounded-lg border py-2 px-1'
        ref={field.ref}
        value={field.value ?? ''}
        onChange={field.onChange}
        onBlur={field.onBlur}
        type={inputType ?? 'text'}
    />
}

export const MyInput: FC<Props> = (props) => {
    return <div className='flex flex-col w-full'>
        <label htmlFor={props.name}>{props.label}</label>
        <Controller
            name={props.name}
            control={props.control}
            render={({ field }) => inputElement(field, props.name, props.inputType)}
        />

    </div>
}