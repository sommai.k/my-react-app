import { FC } from 'react';

interface Props {
    label: string;
    whenClick: () => void;
}

export const MyButton: FC<Props> = (props) => {
    return <>
        <button
            className='bg-blue-800 text-white p-2 w-full rounded-xl hover:bg-blue-600'
            onClick={props.whenClick}
        >{props.label}</button>
    </>
}