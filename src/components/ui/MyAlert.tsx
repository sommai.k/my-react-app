import { FC } from 'react';
import { MyButton } from './MyButton';
import { XMarkIcon } from '@heroicons/react/24/solid'

interface Props {
    title: string;
    content: string;
    whenClose: () => void;
}

export const MyAlert: FC<Props> = (props) => {

    return <>
        <div className='absolute top-0 left-0 w-full h-full flex z-10 bg-gray-500 bg-opacity-60 sm:items-center sm:justify-center'>
            <div className='w-[400px] bg-white rounded-lg shadow-lg p-4 space-y-4'>
                <div className='flex flex-row relative'>
                    <div className='text-2xl'>{props.title}</div>
                    <XMarkIcon className='w-[24px] absolute right-0 hover:text-blue-500 cursor-pointer font-extrabold' onClick={props.whenClose} />
                </div>
                <div className='h-[200px] bg-gray-50'>{props.content}</div>
                <div>
                    <MyButton label="Close" whenClick={props.whenClose} />
                </div>
            </div>
        </div>
    </>

}