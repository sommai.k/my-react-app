import { FC } from 'react';
import logo from '../../assets/node-logo.png';
import { Outlet } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

export const MainApp: FC = () => {
    const navigate = useNavigate();

    const goto = (path: string) => {
        navigate(path);
    }
    return <>
        <div className='flex flex-col w-screen h-screen'>
            <div className='sticky top-0 flex flex-row w-full bg-blue-800 p-4 space-x-8'>
                <div>
                    <img src={logo} className='h-[50px]' />
                </div>

                <div className='flex-1 flex flex-row w-full items-center space-x-6 relative'>
                    <div className='text-2xl text-gray-50'>
                        Demo application
                    </div>
                    <div className='text-gray-300 hover:underline cursor-pointer hover:text-gray-50'
                        onClick={goto.bind(this, 'home')}>Home</div>
                    <div className='text-gray-300 hover:underline cursor-pointer hover:text-gray-50'
                        onClick={goto.bind(this, 'menu')}>Menu</div>
                    <div className='text-gray-300 hover:underline cursor-pointer hover:text-gray-50'
                        onClick={goto.bind(this, 'message')}>Message</div>
                    <div className='text-gray-300 hover:underline cursor-pointer hover:text-gray-50 right-1 absolute'
                        onClick={goto.bind(this, 'setting')}>Setting</div>
                </div>
            </div>
            <div>
                <Outlet />
            </div>
        </div>
    </>
}