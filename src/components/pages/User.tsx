import { FC, useState, useEffect } from 'react';
import axios from 'axios';
import { MyButton } from '../ui/MyButton';
import { useNavigate } from 'react-router-dom';

const api = axios.create();

interface UserModel {
    id: number;
    name: string;
    email: string;
    phone: string;
}

export const User: FC = () => {
    const [data, setData] = useState<UserModel[]>([]);
    const navigate = useNavigate();

    const loadData = async () => {
        try {
            const resp = await api.get('https://jsonplaceholder.typicode.com/users');
            setData(resp.data);
        } catch (e) {
            console.log(e);
        }
    }

    const viewAlbum = (userId: number) => {
        navigate(`/album/${userId}`);
    }

    const renderUser = () => {
        if (data && data.length > 0) {
            return data.map((user) => (
                <div className='flex flex-row space-x-4 items-center bg-blue-50 odd:bg-blue-100 p-4'>
                    <div className='w-[50px] h-[50px]'>
                        <img src="https://via.placeholder.com/100/92c952" className='rounded-full' />
                    </div>
                    <div className='flex-1'>
                        <div className='text-lg'>{user.name}</div>
                        <div className='text-sm'>{user.email}</div>
                        <div className='text-sm'>{user.phone}</div>
                        <MyButton label='Albums' whenClick={viewAlbum.bind(this, user.id)} />
                    </div>
                </div>
            ));
        } else {
            return <></>
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    return <>{renderUser()}</>
}