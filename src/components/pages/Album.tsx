import { FC, useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';

const api = axios.create();

interface AlbumModel {
    id: number;
    title: string;
}

export const Album: FC = () => {
    const params = useParams();
    const [data, setData] = useState<AlbumModel[]>([]);
    const navigate = useNavigate();

    const loadData = async () => {
        try {
            const resp = await api.get(`https://jsonplaceholder.typicode.com/albums?userId=${params['userId']}`);
            setData(resp.data);
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    const viewPhoto = (albumId: number) => {
        navigate(`/photo/${albumId}`);
    }

    return <>
        <div className='w-screen h-screen grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4  gap-4 p-4'>
            {
                data.map((model, index) => (
                    <div key={index} onClick={viewPhoto.bind(this, model.id)}
                        className='bg-yellow-400 rounded-2xl text-center align-middle flex cursor-pointer hover:bg-yellow-300'>
                        <div className='m-auto text-xl font-bold'>{model.title}</div>
                    </div>
                ))
            }
        </div>
    </>
}