import { FC, useState } from 'react';
import banner from '../../assets/banner.jpg';
import logo from '../../assets/node-logo.png';
import { z } from 'zod';
import { SubmitErrorHandler, SubmitHandler, useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { MyInput } from '../ui/MyInput';
import { MyButton } from '../ui/MyButton';
import { MyAlert } from '../ui/MyAlert';
import { useDispatch } from 'react-redux';
import { login } from '../../app/auth.slice';

const LoginSchema = z.object({
    email: z.string().email(),
    password: z.string().min(5)
});

type LoginModel = z.infer<typeof LoginSchema>;

export const Login: FC = () => {
    const [show, setShow] = useState(false);
    const [title, setTitle] = useState("");
    const [content, setContent] = useState("");
    const dispatch = useDispatch();

    const { handleSubmit, control, reset } = useForm<LoginModel>({
        defaultValues: {
            email: '',
            password: ''
        },
        resolver: zodResolver(LoginSchema)
    });

    // when validate pass
    const whenValidatePass: SubmitHandler<LoginModel> = (model) => {
        dispatch(login("abdcefg"));
    }

    // when validate fail
    const whenValidateFail: SubmitErrorHandler<LoginModel> = (errors) => {
        setTitle("พบข้อผิดพลาด");
        setContent("กรุณาตรวจสอบข้อมูล");
        setShow(true);
    }

    // when reset
    const whenReset = () => {
        reset();
    }
    return <>
        <div className='w-screen h-screen flex flex-col bg-gray-50'>
            <div className='m-auto flex flex-row rounded-xl shadow-md md:w-1/2 p-4 space-x-3'>
                <div className='flex-1 flex flex-col items-center p-2 space-y-2'>
                    <img src={logo} className='w-[150px]' />
                    <MyInput label='อีเมล์' name='email' control={control} />
                    <MyInput label='รหัสผ่าน' name='password' control={control} inputType='password' />
                    <div className='flex flex-row w-full space-x-1'>
                        <MyButton label='เข้าสู่ระบบ' whenClick={handleSubmit(whenValidatePass, whenValidateFail)} />
                        <MyButton label='ยกเลิก' whenClick={whenReset} />
                    </div>
                </div>
                <div className=''>
                    <img src={banner} className='w-[300px]' />
                </div>
            </div>
        </div>
        {show && <MyAlert title={title} content={content} whenClose={() => setShow(false)} />}

    </>
}