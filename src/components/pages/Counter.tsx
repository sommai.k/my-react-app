import { FC, useState } from 'react';
import { BigText } from '../ui/BigText';
import { MyButton } from '../ui/MyButton';

export const Counter: FC = () => {
    // declare
    const [cnt, setCnt] = useState(0);

    // custom function
    // when add click
    const whenAddClick = () => {
        setCnt(cnt + 1);
    }

    // render
    return <>
        <div className='space-y-2'>
            <BigText text={`${cnt}`} />
            <MyButton label='Add' whenClick={whenAddClick} />
        </div>
    </>
}