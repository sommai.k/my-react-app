import { FC } from 'react';
import { MyButton } from '../ui/MyButton';
import { useNavigate } from 'react-router-dom';

export const PageNotFound: FC = () => {
    const navigate = useNavigate();

    const goHome = () => {
        navigate('/home');
    }

    return <>
        <div className='w-screen h-screen bg-blue-100 flex flex-col'>
            <div className='m-auto space-y-3'>
                <div className='text-6xl text-center'>404</div>
                <div className='text-xl'>Page Not Found</div>
                <MyButton label='Go Home' whenClick={goHome} />
            </div>
        </div>
    </>
}