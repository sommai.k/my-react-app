import { FC, useCallback } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../app/store';
import { MainApp } from './MainApp';
import { Login } from './Login';

export const Landing: FC = () => {
    const isLogedIn = useSelector<RootState>((state) => state.auth.isLogedIn);

    const showScreen = useCallback(() => {
        if (isLogedIn) {
            return <MainApp />
        } else {
            return <Login />
        }
    }, [isLogedIn]);

    return <>{showScreen()}</>
}