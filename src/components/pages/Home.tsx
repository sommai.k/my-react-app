import { FC, useState, useEffect } from 'react';
import axios from 'axios';
import { CheckCircleIcon, EllipsisHorizontalCircleIcon } from '@heroicons/react/24/outline';
import { MyAlert } from '../ui/MyAlert';
import { User } from './User';

const api = axios.create({});

interface TodoModel {
    title: string;
    completed: boolean;
}

export const Home: FC = () => {
    const [data, setData] = useState<TodoModel[]>([]);
    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');
    const [show, setShow] = useState(false);

    const loaData = async () => {
        try {
            const resp = await api.get('https://jsonplaceholder.typicode.com/todos');
            setData(resp.data);
        } catch (e) {
            setTitle("พบข้อผิดพลาด");
            setContent("กรุณาลองอีกครั้งหนึ่ง");
            setShow(true);
        }
    }

    useEffect(() => {
        loaData();
    }, []);

    const renderTodo = () => {
        return data.map((model, index) => (
            <div key={index} className='flex flex-row p-2 space-x-4'>
                <div>
                    {
                        model.completed ?
                            <CheckCircleIcon className='w-8 h-8 text-green-500' /> :
                            <EllipsisHorizontalCircleIcon className='w-8 h-8' />
                    }
                </div>
                <div className='text-lg'>{model.title}</div>
            </div>
        ));
    }

    return <>
        <div className='p-4 flex flex-row flex-wrap gap-2'>
            <div className='flex flex-col w-full md:w-1/2 lg:w-1/3 bg-gray-100 shadow-lg'>
                <div className='bg-blue-700 text-white text-xl p-2 rounded-t-lg'>งานค้าง</div>
                <div className='h-[300px] overflow-scroll'>
                    {renderTodo()}
                </div>
            </div>
            <div className='flex flex-col w-full md:w-1/2 lg:w-1/3 bg-gray-100 shadow-lg'>
                <div className='h-[350px] overflow-scroll'>
                    <User />
                </div>
            </div>
        </div>

        {show && <MyAlert title={title} content={content} whenClose={setShow.bind(this, false)} />}
    </>
}