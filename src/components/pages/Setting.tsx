import { FC, ReactNode } from 'react';
import { MyButton } from '../ui/MyButton';
import { useDispatch } from 'react-redux';
import { logout } from '../../app/auth.slice';
import { UserCircleIcon, UserGroupIcon } from '@heroicons/react/24/solid';

interface ItemProps {
    title: string;
    subTitle: string;
    icon: ReactNode
}

const SettingItem: FC<ItemProps> = (props) => {
    return <>
        <div className='flex flex-row bg-gray-200 p-2 rounded-lg hover:bg-gray-300 cursor-pointer space-x-2 items-center'>
            <div className='h-[48px] w-[48px] text-blue-400'>
                {props.icon}
            </div>
            <div className='flex flex-col space-y-2'>
                <div className='text-xl text-blue-800'>{props.title}</div>
                <div className='text-sm text-blue-800'>{props.subTitle}</div>
            </div>
        </div>
    </>
}

export const Setting: FC = () => {
    const dispatch = useDispatch();

    const whenLogOut = () => {
        dispatch(logout());
    }

    return <>
        <div className='flex flex-col p-8 space-y-2'>
            <div className='text-xl'>ตั้งค่าระบบ</div>
            <div className='grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-2'>
                <SettingItem title='ผู้ใช้งาน' subTitle='ตั้งค่าผู้ใช้งาน' icon={<UserCircleIcon />} />
                <SettingItem title='กลุ่มผู้ใช้งาน' subTitle='ตั้งค่ากลุ่มผู้ใช้งาน' icon={<UserGroupIcon />} />
            </div>
            <div className='max-w-[150px]'>
                <MyButton label='ออกจากระบบ' whenClick={whenLogOut} />
            </div>
        </div>
    </>
}