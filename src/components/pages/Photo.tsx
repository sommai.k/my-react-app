import { FC, useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';

const api = axios.create();

interface PhotoModel {
    id: number;
    title: string;
    thumbnailUrl: string;
}

export const Photo: FC = () => {
    const [data, setData] = useState<PhotoModel[]>([]);
    const params = useParams();

    const loadData = async () => {
        try {
            const resp = await api.get(`https://jsonplaceholder.typicode.com/photos?albumId=${params['albumId']}`);
            setData(resp.data);
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    const renderPhoto = () => {
        return data.map((photo) => (
            <div className='flex flex-col border rounded-lg shadow-md'>
                <img src={photo.thumbnailUrl} />
                <div className='p-2 text-lg'>{photo.title}</div>
            </div>
        ));
    }

    return <>
        <div className='w-screen h-screen p-4'>
            <div className='grid grid-cols-4 gap-4'>
                {renderPhoto()}
            </div>
        </div>
    </>
}