import { FC, useState, useEffect } from 'react';
import { MyAlert } from '../ui/MyAlert';
import { ArrowPathIcon } from '@heroicons/react/24/solid';

interface PostItem {
    userId: number;
    id: number;
    title: string;
    body: string;
}

export const Message: FC = () => {
    const [data, setData] = useState<PostItem[]>([]);
    const [loading, setLoading] = useState(false);
    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');
    const [show, setShow] = useState(false);

    useEffect(() => {
        loadData();
    }, [])

    const loadData = async () => {
        try {
            setLoading(true);
            const resp = await fetch('https://jsonplaceholder.typicode.com/posts');
            if (resp.ok) {
                const json = await resp.json();
                setData(json);
            } else {
                setTitle('มีปัญหา');
                setContent('กรุณาลองอีกครั้ง');
                setShow(true);
            }
        } catch (e) {
            setTitle('มีปัญหา');
            setContent('กรุณาลองอีกครั้ง');
            setShow(true);
        } finally {
            setLoading(false);
        }
    }

    const renderItem = () => {
        if (data && data.length > 0) {
            return data.map((value) =>
                <tr className='bg-blue-100 odd:bg-blue-200 h-[35px]'>
                    <td className='px-2 text-center'>{value.userId}</td>
                    <td>{value.id}</td>
                    <td>{value.title}</td>
                    <td>{value.body}</td>
                </tr>
            )
        } else {
            return <></>
        }
    }

    return <>
        <div className='h-[500px] flex'>
            <table className='w-full max-h-[500px]'>
                <thead className='bg-blue-700 text-white'>
                    <tr className=''>
                        <th className='w-[150px] p-4'>User Id</th>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Body</th>
                    </tr>
                </thead>
                <tbody>
                    {loading &&
                        <div className='animate-spin w-[36px] h-[36px]'><ArrowPathIcon className='' /></div>}
                    {renderItem()}
                </tbody>
            </table>
        </div >
        {show && <MyAlert title={title} content={content} whenClose={setShow.bind(this, false)} />}
    </>
}