import { FC } from 'react';

interface MenuModel {
    name: string;
    item: MenuItem[]
}

interface MenuItem {
    name: string;
    path: string;
}

const menuData: MenuModel[] = [
    {
        name: 'สินค้าคงคลัง',
        item: [{
            name: 'นำเข้าข้อมูล',
            path: 'A'
        },
        {
            name: 'ตรวจนับสินค้า',
            path: 'A'
        }
        ]
    }, {
        name: 'รายการขาย',
        item: [{
            name: 'บันทึกงานขาย',
            path: 'A'
        },
        {
            name: 'ยกเลิกรายการ',
            path: 'A'
        },
        {
            name: 'ออกบิล',
            path: 'A'
        }
        ]
    }, {
        name: 'รายงาน',
        item: [{
            name: 'ยอดขายประจำวัน',
            path: 'A'
        },
        {
            name: 'ยอดขายตามไตรมาส',
            path: 'A'
        },
        {
            name: 'คอมมิทชั่น',
            path: 'A'
        }
        ]
    }
];

export const Menu: FC = () => {

    const goto = (path: string) => {
        console.log(path);
    }

    const renderMenu = () => {
        return menuData.map((model) => (
            <div className='w-full md:w-1/2 lg:w-1/3 xl:w-1/4'>
                <div className='rounded-2xl border p-4 shadow-lg space-y-4 m-2'>
                    <div className='text-xl font-bold'>{model.name}</div>
                    <div className='h-[300px] px-8 space-y-2'>
                        {model.item.map((item) =>
                            <div
                                className='text-blue-700 hover:underline cursor-pointer'
                                onClick={goto.bind(this, item.path)}
                            >{item.name}</div>
                        )}
                    </div>
                </div>
            </div>
        ));
    }

    return <>
        <div className='p-4 flex flex-row flex-wrap'>
            {renderMenu()}
        </div>
    </>
}