import { PayloadAction, createSlice } from '@reduxjs/toolkit';

const authSlice = createSlice({
    name: 'auth',
    initialState: {
        isLogedIn: sessionStorage.getItem('isLogedIn') ? true : false,
        token: sessionStorage.getItem('token') ?? ''
    },
    reducers: {
        login: (state, action: PayloadAction<string>) => {
            state.isLogedIn = true;
            state.token = action.payload;
            sessionStorage.setItem('isLogedIn', 'true');
            sessionStorage.setItem('token', action.payload);
        },
        logout: (state) => {
            state.isLogedIn = false;
            state.token = '';
            sessionStorage.removeItem('isLogedIn');
            sessionStorage.removeItem('token');
        }
    }
});

export const authReducer = authSlice.reducer;
export const { login, logout } = authSlice.actions;