import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'
import { store } from './app/store.ts';
import { Provider } from 'react-redux';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { PageNotFound } from './components/pages/PageNotFound.tsx';
import { Home } from './components/pages/Home.tsx';
import { Menu } from './components/pages/Menu.tsx';
import { Message } from './components/pages/Message.tsx';
import { Setting } from './components/pages/Setting.tsx';
import { Album } from './components/pages/Album.tsx';
import { Photo } from './components/pages/Photo.tsx';

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [{
      path: 'home',
      element: <Home />
    }, {
      path: 'menu',
      element: <Menu />
    }, {
      path: 'message',
      element: <Message />
    }, {
      path: 'setting',
      element: <Setting />
    }]
  },
  {
    path: '/album/:userId',
    element: <Album />
  },
  {
    path: '/photo/:albumId',
    element: <Photo />
  },
  {
    path: '*',
    element: <PageNotFound />
  }
]);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>,
)
